import 'package:flutter/material.dart';
import 'package:flutterweatherapp/constants/values/route_constants.dart';
import 'package:flutterweatherapp/ui/pages/home/home_page.dart';
import 'package:flutterweatherapp/ui/pages/splash/splash_page.dart';

import 'cupertino_style_navigation_route.dart';

class MainRouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case AppRoutes.splashPage:
        return CustomCupertinoStyleNavigationRoute(
          builder: (_) => const SplashPage(),
        );
      case AppRoutes.homePage:
        return MaterialPageRoute<dynamic>(
          builder: (context) => const HomePage(),
          settings: settings,
        );
      default:
        return MaterialPageRoute<dynamic>(
          builder: (context) => const SplashPage(),
          settings: settings,
        );
    }
  }
}
