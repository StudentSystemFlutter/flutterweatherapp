import 'package:flutterweatherapp/constants/assets/icon_constants.dart';
import 'package:flutterweatherapp/utils/validator/validators.dart';

String getWeatherAppIcon({required String? iconName}) {
  if (!Validators.isStringNotEmpty(iconName)) {
    return AppIcons.w01d;
  }

  switch (iconName) {
    case '01d':
      return AppIcons.w01d;
    case '02d':
      return AppIcons.w02d;
    case '03d':
      return AppIcons.w03d;
    case '04d':
      return AppIcons.w04d;
    case '09d':
      return AppIcons.w09d;
    case '10d':
      return AppIcons.w10d;
    case '11d':
      return AppIcons.w11d;
    case '13d':
      return AppIcons.w13d;
    case '50d':
      return AppIcons.w50d;
    case '01n':
      return AppIcons.w01n;
    case '02n':
      return AppIcons.w02n;
    case '03n':
      return AppIcons.w03n;
    case '04n':
      return AppIcons.w04n;
    case '09n':
      return AppIcons.w09n;
    case '10n':
      return AppIcons.w10n;
    case '11n':
      return AppIcons.w11n;
    case '13n':
      return AppIcons.w13n;
    case '50n':
      return AppIcons.w50n;

    default:
      return AppIcons.w01d;
  }
}
