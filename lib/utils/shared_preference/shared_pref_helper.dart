import 'package:flutterweatherapp/constants/values/shared_pref_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefHelper {
  static SharedPreferences? prefs;
  static Future<void> init() async {
    prefs ??= prefs = await SharedPreferences.getInstance();
  }

  static void dispose() => prefs = null;
  static Future<bool> clear() => prefs!.clear();

  static String? getLanguage() => prefs!.getString(ShPrefKeys.language);
  static set language(String value) =>
      prefs!.setString(ShPrefKeys.language, value);

  static String? getUserDisplayName() =>
      prefs!.getString(ShPrefKeys.userDisplayName);
  static set userDisplayName(String value) =>
      prefs!.setString(ShPrefKeys.userDisplayName, value);
}
