import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

abstract class LocalizationUtil {
  static const path = 'assets/locales';
  static const List<String> supportedLanguages = ['English', 'Русский'];
  static const List<String> supportedLanguagesCodes = ['en', 'ru'];
  static const List<String> supportedLanguagesCountryCodes = ['US', 'RU'];

  static List<Locale> supportedLocales() => supportedLanguagesCodes
      .map<Locale>((language) => Locale(
          language,
          supportedLanguagesCountryCodes[
              supportedLanguagesCodes.indexOf(language)]))
      .toList();

  static String currentLangCode(BuildContext context) {
    String currentLocale = context.locale.toString();
    if (currentLocale.length > 2) {
      if (currentLocale[2] == '-' || currentLocale[2] == '_') {
        currentLocale = currentLocale.substring(0, 2);
      }
    }
    return currentLocale;
  }

  static String currentLang(BuildContext context) {
    String currentLocale = context.locale.toString();
    String currentLanuage = '';
    if (currentLocale.length > 2) {
      if (currentLocale[2] == '-' || currentLocale[2] == '_') {
        currentLocale = currentLocale.substring(0, 2);
      }
    }
    if (supportedLanguagesCodes.any((element) => element == currentLocale) &&
        supportedLanguagesCodes.length == supportedLanguages.length) {
      currentLanuage =
          supportedLanguages[supportedLanguagesCodes.indexOf(currentLocale)];
    }
    return currentLanuage;
  }

  static void updateLocal(BuildContext context, String lang) {
    context.setLocale(Locale(lang,
        supportedLanguagesCountryCodes[supportedLanguagesCodes.indexOf(lang)]));
  }
}
