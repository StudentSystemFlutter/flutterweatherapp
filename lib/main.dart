import 'package:bot_toast/bot_toast.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutterweatherapp/bloc/auth/auth_bloc.dart';
import 'package:flutterweatherapp/bloc/home/home_bloc.dart';
import 'package:flutterweatherapp/constants/values/color_constants.dart';
import 'package:flutterweatherapp/utils/localization/localization_util.dart';
import 'package:flutterweatherapp/utils/navigator/locator.dart';
import 'package:flutterweatherapp/utils/navigator/main_route_generator.dart';
import 'package:flutterweatherapp/utils/navigator/navigation_service.dart';
import 'package:flutterweatherapp/utils/shared_preference/shared_pref_helper.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // Initializing Localization
  await EasyLocalization.ensureInitialized();

  // Initializing Shared Preferebces
  await SharedPrefHelper.init();

  // For logging API requests on dev console
  setupLocator();
  setUpLogging();

  // For Portrait mode only
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(
    EasyLocalization(
      supportedLocales: LocalizationUtil.supportedLocales(),
      path: LocalizationUtil.path,
      fallbackLocale: const Locale('en', 'US'),
      child: const WeatherApp(),
    ),
  );
}

class WeatherApp extends StatefulWidget {
  const WeatherApp({Key? key}) : super(key: key);

  @override
  _WeatherAppState createState() => _WeatherAppState();
}

class _WeatherAppState extends State<WeatherApp> {
  final NavigationService navigationService = locator<NavigationService>();

  @override
  void initState() {
    super.initState();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
  }

  @override
  void dispose() {
    SharedPrefHelper.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = ThemeData();

    return ScreenUtilInit(
      designSize: const Size(360, 640),
      builder: () => MultiBlocProvider(
        providers: [
          BlocProvider(create: (context) => AuthBloc()),
          BlocProvider(create: (context) => HomeBloc()),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          builder: BotToastInit(),
          locale: context.locale,
          localizationsDelegates: context.localizationDelegates,
          supportedLocales: LocalizationUtil.supportedLocales(),
          navigatorObservers: [BotToastNavigatorObserver()],
          theme: theme.copyWith(
            primaryColor: AppColors.accent,
            colorScheme: theme.colorScheme.copyWith(
              secondary: AppColors.accent,
            ),
            brightness: Brightness.light,
            applyElevationOverlayColor: false,
            backgroundColor: AppColors.white,
            canvasColor: Colors.transparent,
            iconTheme: const IconThemeData(color: AppColors.iconColor),
            inputDecorationTheme: const InputDecorationTheme(
              floatingLabelBehavior: FloatingLabelBehavior.never,
              enabledBorder: OutlineInputBorder(),
              labelStyle: TextStyle(color: AppColors.red),
            ),
          ),
          onGenerateRoute: MainRouteGenerator.generateRoute,
          navigatorKey: locator<NavigationService>().navigatorKey,
        ),
      ),
    );
  }
}
