import 'package:chopper/chopper.dart';
import 'package:flutterweatherapp/constants/values/app_constants.dart';
import 'package:flutterweatherapp/constants/values/url_constants.dart';
import 'package:flutterweatherapp/models/api/weather_full_data/weather_full_data.dart';

part 'weather_service.chopper.dart';

@ChopperApi(baseUrl: AppUrls.base)
abstract class WeatherService extends ChopperService {
  static WeatherService create([ChopperClient? client]) =>
      _$WeatherService(client ?? ChopperClient());

  @Get(
    path:
        'onecall?appid=${AppConstants.openWeatherKey}&lang={lang}&exclude=minutely,alerts&lat={lat}&lon={long}&units=metric',
  )
  Future<Response<WeatherFullDataResponse>>? getCurrentCityWeatherData(
    @Path('lang') String lang,
    @Path('lat') double lat,
    @Path('long') double long,
  );
}
