// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$WeatherService extends WeatherService {
  _$WeatherService([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = WeatherService;

  @override
  Future<Response<WeatherFullDataResponse>> getCurrentCityWeatherData(
      String lang, double lat, double long) {
    final $url =
        'https://api.openweathermap.org/data/2.5/onecall?appid=335f05f6025d5463f8e15626daab5a12&lang=${lang}&exclude=minutely,alerts&lat=${lat}&lon=${long}&units=metric';
    final $request = Request('GET', $url, client.baseUrl);
    return client
        .send<WeatherFullDataResponse, WeatherFullDataResponse>($request);
  }
}
