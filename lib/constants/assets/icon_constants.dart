class AppIcons {
  static const base = 'assets/icons';
  static const apple = '$base/apple.svg';
  static const google = '$base/google.svg';

  // Weather icons
  // Day icons
  static const weather = '$base/weather';
  static const w01d = '$weather/01d.png';
  static const w02d = '$weather/02d.png';
  static const w03d = '$weather/03d.png';
  static const w04d = '$weather/04d.png';
  static const w09d = '$weather/09d.png';
  static const w10d = '$weather/10d.png';
  static const w11d = '$weather/11d.png';
  static const w13d = '$weather/13d.png';
  static const w50d = '$weather/50d.png';

  // Night icons
  static const w01n = '$weather/01n.png';
  static const w02n = '$weather/02n.png';
  static const w03n = '$weather/03n.png';
  static const w04n = '$weather/04n.png';
  static const w09n = '$weather/09n.png';
  static const w10n = '$weather/10n.png';
  static const w11n = '$weather/11n.png';
  static const w13n = '$weather/13n.png';
  static const w50n = '$weather/50n.png';
}
