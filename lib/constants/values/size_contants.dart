import 'package:flutter_screenutil/flutter_screenutil.dart';

class SizeConstants {
  static final dynamicSize = ScreenUtil();

  static final double textSize8 = dynamicSize.setSp(8);
  static final double textSize9 = dynamicSize.setSp(9);
  static final double textSize10 = dynamicSize.setSp(10);
  static final double textSize11 = dynamicSize.setSp(11);
  static final double textSize12 = dynamicSize.setSp(12);
  static final double textSize13 = dynamicSize.setSp(13);
  static final double textSize14 = dynamicSize.setSp(14);
  static final double textSize15 = dynamicSize.setSp(15);
  static final double textSize16 = dynamicSize.setSp(16);
  static final double textSize17 = dynamicSize.setSp(17);
  static final double textSize18 = dynamicSize.setSp(18);
  static final double textSize20 = dynamicSize.setSp(20);
  static final double textSize21 = dynamicSize.setSp(21);

  static final double textSize22 = dynamicSize.setSp(22);
  static final double textSize24 = dynamicSize.setSp(24);
  static final double textSize28 = dynamicSize.setSp(28);
  static final double textSize30 = dynamicSize.setSp(30);
  static final double textSize32 = dynamicSize.setSp(32);
  static final double textSize34 = dynamicSize.setSp(34);
  static final double textSize36 = dynamicSize.setSp(36);
  static final double textSize40 = dynamicSize.setSp(40);
  static final double textSize45 = dynamicSize.setSp(45);
  static final double textSize48 = dynamicSize.setSp(48);
  static final double textSize55 = dynamicSize.setSp(55);
  static final double textSize58 = dynamicSize.setSp(58);
  static final double textSize90 = dynamicSize.setSp(90);
}
