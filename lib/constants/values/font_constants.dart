import 'package:flutter/material.dart';
import 'package:flutterweatherapp/constants/values/color_constants.dart';
import 'package:flutterweatherapp/constants/values/size_contants.dart';

class AppFonts {
  static const regular = 'Montserrat';
  static const black = 'Montserrat-Black';
  static const semibold = 'Montserrat-SemiBold';
  static const medium = 'Montserrat-Medium';
  static const bold = 'Montserrat-Bold';
}

class AppTypography {
  static final font8Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    fontSize: SizeConstants.textSize8,
    color: AppColors.black,
  );

  static final font9Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    fontSize: SizeConstants.textSize9,
    color: AppColors.black,
  );

  static final font10Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    fontSize: SizeConstants.textSize10,
    color: AppColors.black,
  );

  static final font11Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    fontSize: SizeConstants.textSize11,
    color: AppColors.black,
  );

  static final font12Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    fontSize: SizeConstants.textSize12,
    color: AppColors.black,
  );

  static final font13Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    fontSize: SizeConstants.textSize13,
    color: AppColors.black,
  );

  static final font14Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    fontSize: SizeConstants.textSize14,
    color: AppColors.black,
  );

  static final font15Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    fontSize: SizeConstants.textSize15,
    color: AppColors.black,
  );

  static final font16Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    fontSize: SizeConstants.textSize16,
    color: AppColors.black,
  );

  static final font17Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    fontSize: SizeConstants.textSize17,
    color: AppColors.black,
  );

  static final font18Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    fontSize: SizeConstants.textSize18,
    color: AppColors.black,
  );

  static final font20Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    fontSize: SizeConstants.textSize20,
    color: AppColors.black,
  );

  static final font21Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    fontSize: SizeConstants.textSize21,
    color: AppColors.black,
  );
  static final font22Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    fontSize: SizeConstants.textSize22,
    color: AppColors.black,
  );

  static final font24Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    fontSize: SizeConstants.textSize24,
    color: AppColors.black,
  );

  static final font32Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    fontSize: SizeConstants.textSize32,
    color: AppColors.black,
  );

  static final font40Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.normal,
    fontSize: SizeConstants.textSize40,
    color: AppColors.black,
  );
}
