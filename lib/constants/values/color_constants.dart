import 'package:flutter/material.dart';

abstract class AppColors {
  static const accent = Color(0xFF7e57c2);
  static const accentDark = Color(0xFF4d2c91);
  static const accentLight = Color(0xFFb085f5);

  static const green = Color(0xFF21A73F);
  static const red = Color(0xFFd32f2f);
  static const black = Color(0xFF000000);
  static const white = Color(0xFFffffff);
  static const textFieldBackground = Color(0xFFF0F0F0);

  static const transparent = Color(0x00FFFFFF);
  static const iconColor = Color(0xFFBABABA);
  static const iconColorAlt = Color(0xFF7F7F7F);

  static const blueColor = Color(0xFF3778FA);
  static const greyDisabled = Color(0xFFE7E7E7);
  static const greyColor = Color(0xFF787878);

  static const backgroundColor = Color(0xFFfcfcfc);
  static const backgroundColorAlt = Color(0xFFE5E5E5);
  static const shadowColor = Color(0xFF146eaf);

  static const focusedBorder = Color(0xFF140F1E);
  static const hintColor = Color(0xFFC4C4C4);
  static const border = Color(0xFFD0D0D0);
  static const errorColor = Color(0xFFFF5454);
  static const dividerColor = Color(0xFFD8D8D8);
}
