import 'dart:io';
import 'package:chopper/chopper.dart';
import 'package:flutterweatherapp/services/weather/weather_service.dart';
import 'package:http/io_client.dart' as http;
import 'custom_converter.dart';

class ApiProvider {
  static late ChopperClient _client;
  static late WeatherService weatherService;

  static create() {
    _client = ChopperClient(
      client: http.IOClient(
        HttpClient()..connectionTimeout = const Duration(seconds: 40),
      ),
      services: [
        WeatherService.create(),
      ],
      interceptors: getInterceptors(),
      converter: CustomDataConverter(),
    );

    weatherService = _client.getService<WeatherService>();
  }

  static List getInterceptors() {
    List interceptors = [];

    interceptors.add(HttpLoggingInterceptor());

    interceptors.add(const HeadersInterceptor({
      HttpHeaders.acceptHeader: 'application/json',
      HttpHeaders.contentTypeHeader: 'application/json',
    }));

    return interceptors;
  }

  static dispose() {
    _client.dispose();
  }
}
