import 'package:chopper/chopper.dart';
import 'package:flutterweatherapp/models/api/current_weather/current_weather.dart';
import 'package:flutterweatherapp/models/api/daily_weather/daily_weather.dart';
import 'package:flutterweatherapp/models/api/feels_like/feels_like.dart';
import 'package:flutterweatherapp/models/api/hourly_weather/hourly_weather.dart';
import 'package:flutterweatherapp/models/api/temperature/temperature.dart';
import 'package:flutterweatherapp/models/api/weather/weather.dart';
import 'package:flutterweatherapp/models/api/weather_full_data/weather_full_data.dart';

class CustomDataConverter extends JsonConverter {
  @override
  Response<BodyType> convertResponse<BodyType, InnerType>(Response response) {
    final Response dynamicResponse = super.convertResponse(response);
    var body = dynamicResponse.body;

    final BodyType customBody =
        convertToCustomObject<BodyType, InnerType>(body);

    return dynamicResponse.copyWith<BodyType>(
      body: customBody,
    );
  }

  BodyType convertToCustomObject<BodyType, SingleItemType>(dynamic element) {
    if (element is List) {
      return deserializeListOf<BodyType, SingleItemType>(element);
    } else {
      return deserialize<SingleItemType>(element);
    }
  }

  dynamic deserializeListOf<BodyType, SingleItemType>(
    List dynamicList,
  ) {
    List<SingleItemType> list = dynamicList
        .map<SingleItemType>((element) => deserialize<SingleItemType>(element))
        .toList();
    return list;
  }

  dynamic deserialize<SingleItemType>(Map<String, dynamic> json) {
    switch (SingleItemType) {
      case WeatherFullDataResponse:
        return WeatherFullDataResponse.fromJson(json);
      case CurrentWeatherResponse:
        return CurrentWeatherResponse.fromJson(json);
      case DailyWeatherResponse:
        return DailyWeatherResponse.fromJson(json);
      case FeelsLikeResponse:
        return FeelsLikeResponse.fromJson(json);
      case HourlyWeatherResponse:
        return HourlyWeatherResponse.fromJson(json);
      case TemperatureResponse:
        return TemperatureResponse.fromJson(json);
      case WeatherResponse:
        return WeatherResponse.fromJson(json);
      default:
        return null;
    }
  }
}
