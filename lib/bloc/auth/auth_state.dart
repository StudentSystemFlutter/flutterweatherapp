part of 'auth_bloc.dart';

class AuthState {
  final String displayName;
  final bool isAuthenticated;
  final bool isLoading;

  const AuthState({
    required this.displayName,
    this.isAuthenticated = false,
    this.isLoading = false,
  });

  AuthState copyWith({
    String? displayName,
    bool? isAuthenticated,
    bool? isLoading,
  }) {
    return AuthState(
      displayName: displayName ?? this.displayName,
      isAuthenticated: isAuthenticated ?? this.isAuthenticated,
      isLoading: isLoading ?? this.isLoading,
    );
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is AuthState &&
        other.displayName == displayName &&
        other.isAuthenticated == isAuthenticated &&
        other.isLoading == isLoading;
  }

  @override
  int get hashCode =>
      displayName.hashCode ^ isAuthenticated.hashCode ^ isLoading.hashCode;
}
