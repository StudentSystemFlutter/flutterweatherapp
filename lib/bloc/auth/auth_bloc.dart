import 'package:bloc/bloc.dart';
import 'package:flutterweatherapp/utils/shared_preference/shared_pref_helper.dart';
import 'package:google_sign_in/google_sign_in.dart';

part 'auth_state.dart';

class AuthBloc extends Cubit<AuthState> {
  AuthBloc() : super(const AuthState(displayName: ''));

  void checkAuth() async {
    var displayName = '';
    final _googleSignIn = GoogleSignIn();

    final isSignedIn = await _googleSignIn.isSignedIn();

    if (isSignedIn) {
      displayName = SharedPrefHelper.getUserDisplayName() ?? '';

      emit(
        state.copyWith(
          isLoading: false,
          isAuthenticated: true,
          displayName: displayName,
        ),
      );
    }
  }

  void signInWithGoogle() async {
    emit(state.copyWith(isLoading: true));

    try {
      var displayName = '';
      final _googleSignIn = GoogleSignIn();

      final isSignedIn = await _googleSignIn.isSignedIn();

      if (isSignedIn) {
        displayName = SharedPrefHelper.getUserDisplayName() ?? '';

        emit(
          state.copyWith(
            isLoading: false,
            isAuthenticated: true,
            displayName: displayName,
          ),
        );
      } else {
        final _googleUser = await _googleSignIn.signIn();

        if (_googleUser != null) {
          displayName = _googleUser.displayName ?? '';

          SharedPrefHelper.userDisplayName = displayName;

          emit(
            state.copyWith(
              isLoading: false,
              isAuthenticated: true,
              displayName: displayName,
            ),
          );
        }
      }
    } catch (e) {
      emit(state.copyWith(isLoading: false));
    }
  }
}
