part of 'home_bloc.dart';

class HomeState {
  final String locationText;
  final WeatherType selectedWeatherType;
  final CurrentWeatherViewModel currentWeatherViewModel;
  final List<HourlyDataViewModel> hourlyDataList;
  final List<DailyDataViewModel> dailyDataList;
  final String errorText;
  final bool isLoading;

  HomeState({
    required this.locationText,
    required this.selectedWeatherType,
    required this.currentWeatherViewModel,
    required this.hourlyDataList,
    required this.dailyDataList,
    this.errorText = '',
    this.isLoading = true,
  });

  HomeState copyWith({
    String? locationText,
    WeatherType? selectedWeatherType,
    CurrentWeatherViewModel? currentWeatherViewModel,
    List<HourlyDataViewModel>? hourlyDataList,
    List<DailyDataViewModel>? dailyDataList,
    String? errorText,
    bool? isLoading,
  }) {
    return HomeState(
      locationText: locationText ?? this.locationText,
      selectedWeatherType: selectedWeatherType ?? this.selectedWeatherType,
      currentWeatherViewModel:
          currentWeatherViewModel ?? this.currentWeatherViewModel,
      hourlyDataList: hourlyDataList ?? this.hourlyDataList,
      dailyDataList: dailyDataList ?? this.dailyDataList,
      errorText: errorText ?? this.errorText,
      isLoading: isLoading ?? this.isLoading,
    );
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is HomeState &&
        other.locationText == locationText &&
        other.selectedWeatherType == selectedWeatherType &&
        other.currentWeatherViewModel == currentWeatherViewModel &&
        listEquals(other.hourlyDataList, hourlyDataList) &&
        listEquals(other.dailyDataList, dailyDataList) &&
        other.errorText == errorText &&
        other.isLoading == isLoading;
  }

  @override
  int get hashCode {
    return locationText.hashCode ^
        selectedWeatherType.hashCode ^
        currentWeatherViewModel.hashCode ^
        hourlyDataList.hashCode ^
        dailyDataList.hashCode ^
        errorText.hashCode ^
        isLoading.hashCode;
  }
}
