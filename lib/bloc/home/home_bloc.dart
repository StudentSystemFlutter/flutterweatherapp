import 'package:bloc/bloc.dart';
import 'package:connectivity/connectivity.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';

import 'package:flutterweatherapp/api/api_provider.dart';
import 'package:flutterweatherapp/constants/assets/icon_constants.dart';
import 'package:flutterweatherapp/models/api/weather_full_data/weather_full_data.dart';
import 'package:flutterweatherapp/models/view/current_weather_view_model.dart';
import 'package:flutterweatherapp/models/view/daily_data_view_model.dart';
import 'package:flutterweatherapp/models/view/hourly_data_view_model.dart';
import 'package:flutterweatherapp/models/view/weather_type.dart';
import 'package:flutterweatherapp/utils/geo_location/geo_location.dart';
import 'package:flutterweatherapp/utils/shared_preference/shared_pref_helper.dart';
import 'package:flutterweatherapp/utils/storage/storage.dart';

part 'home_state.dart';

class HomeBloc extends Cubit<HomeState> {
  HomeBloc()
      : super(
          HomeState(
            locationText: '',
            selectedWeatherType: WeatherType(type: 0, text: tr('hourly')),
            currentWeatherViewModel: CurrentWeatherViewModel(
              iconName: AppIcons.w01d,
              weatherStatus: '-',
              temperature: '0',
              humidity: '0',
              pressure: '0',
              clouds: '0',
              windSpeed: '0',
            ),
            hourlyDataList: [],
            dailyDataList: [],
          ),
        );

  void loadWeatherDataForCurrentLocation() async {
    emit(state.copyWith(isLoading: true));

    var connectivityResult = await (Connectivity().checkConnectivity());

    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      final lang = SharedPrefHelper.getLanguage() ?? 'en';

      // Initial London location
      var lat = 51.5072;
      var long = 0.1276;

      try {
        final currentLocation = await getLocationData();
        lat = currentLocation.latitude;
        long = currentLocation.longitude;
      } catch (e) {
        final error = e.toString();
        debugPrint(error);
      }

      // GET ONLINE DATA FROM INTERNET
      final response = await ApiProvider.weatherService
          .getCurrentCityWeatherData(lang, lat, long);

      if (response != null && response.statusCode == 200) {
        final weatherData = response.body;

        if (weatherData != null) {
          saveWeatherData(weatherData); // Store data for offline use

          prepareDataForUI(weatherData);
        }
      }
    } else {
      // GET OFFLINE DATA FROM THE STORAGE
      final weatherData = await readWeatherData();
      prepareDataForUI(weatherData);
    }
  }

  void prepareDataForUI(WeatherFullDataResponse? weatherData) {
    final hourlyDataList = <HourlyDataViewModel>[];
    final dailyDataList = <DailyDataViewModel>[];
    final lang = SharedPrefHelper.getLanguage() ?? 'en';

    if (weatherData != null) {
      final currentWeatherViewModel = CurrentWeatherViewModel(
        iconName: weatherData.current?.weather?.first.icon ?? AppIcons.w01d,
        weatherStatus: weatherData.current?.weather?.first.description ?? '',
        temperature: (weatherData.current?.temp?.truncate().toString()) ?? '0',
        humidity: (weatherData.current?.humidity ?? 0).toString(),
        pressure: (weatherData.current?.pressure ?? 0).toString(),
        clouds: (weatherData.current?.clouds ?? 0).toString(),
        windSpeed: (weatherData.current?.windSpeed ?? 0.0).toString(),
      );

      if (weatherData.hourly != null) {
        for (var hourlyData in weatherData.hourly!) {
          final date =
              DateTime.fromMillisecondsSinceEpoch((hourlyData.dt ?? 0) * 1000);

          final day = DateFormat.MMMd(lang).format(date);
          final time = DateFormat.Hm(lang).format(date);

          final hourlyDataViewModel = HourlyDataViewModel(
            iconName: hourlyData.weather?.first.icon ?? AppIcons.w01d,
            day: day,
            time: time,
            weatherStatus: hourlyData.weather?.first.description ?? '',
            temperature: (hourlyData.temp?.truncate().toString()) ?? '0',
            humidity: hourlyData.humidity.toString(),
            pressure: hourlyData.pressure.toString(),
            clouds: hourlyData.clouds.toString(),
            visibility: hourlyData.visibility.toString(),
            windSpeed: hourlyData.windSpeed.toString(),
          );

          hourlyDataList.add(hourlyDataViewModel);
        }
      }

      if (weatherData.daily != null) {
        for (var dailyData in weatherData.daily!) {
          final date =
              DateTime.fromMillisecondsSinceEpoch((dailyData.dt ?? 0) * 1000);

          final day = DateFormat.MMMd(lang).format(date);

          final dailyDataViewModel = DailyDataViewModel(
            iconName: dailyData.weather?.first.icon ?? AppIcons.w01d,
            day: day,
            weatherStatus: dailyData.weather?.first.description ?? '',
            dayTemperature: (dailyData.temp?.day?.truncate().toString()) ?? '0',
            nightTemperature:
                (dailyData.temp?.night?.truncate().toString()) ?? '0',
            morningTemperature:
                (dailyData.temp?.morn?.truncate().toString()) ?? '0',
            eveningTemperature:
                (dailyData.temp?.eve?.truncate().toString()) ?? '0',
            humidity: dailyData.humidity.toString(),
            pressure: dailyData.pressure.toString(),
            clouds: dailyData.clouds.toString(),
            sunrise: dailyData.sunrise.toString(),
            sunset: dailyData.sunset.toString(),
            moonrise: dailyData.moonrise.toString(),
            moonset: dailyData.moonset.toString(),
            windSpeed: dailyData.windSpeed.toString(),
          );

          dailyDataList.add(dailyDataViewModel);
        }
      }

      emit(state.copyWith(
        isLoading: false,
        locationText: weatherData.timezone,
        currentWeatherViewModel: currentWeatherViewModel,
        hourlyDataList: hourlyDataList,
        dailyDataList: dailyDataList,
      ));
    }
  }

  void changeWeatherType(WeatherType type) {
    emit(state.copyWith(selectedWeatherType: type));
  }
}
