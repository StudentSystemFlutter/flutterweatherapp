import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutterweatherapp/constants/values/color_constants.dart';
import 'package:flutterweatherapp/constants/values/font_constants.dart';

import '../texts/app_text.dart';

class DefaultButton extends StatelessWidget {
  final String? text;
  final Widget? child;
  final bool isFilled;
  final Color? backgroundColor;
  final double? height;
  final bool enabled;
  final VoidCallback onPressed;

  const DefaultButton({
    Key? key,
    this.child,
    this.isFilled = true,
    required this.onPressed,
    this.backgroundColor,
    this.text,
    this.height,
    this.enabled = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: enabled ? 1 : .5,
      child: MaterialButton(
        onPressed: () {
          if (enabled) {
            onPressed();
          }
        },
        focusElevation: 0,
        hoverElevation: 0,
        highlightElevation: 0,
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        padding: const EdgeInsets.all(0),
        color: backgroundColor ??
            (isFilled ? AppColors.accentDark : AppColors.textFieldBackground),
        child: Container(
          height: height ?? 48.h,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            boxShadow: backgroundColor == null && isFilled
                ? [
                    BoxShadow(
                      color: Colors.black.withOpacity(.1),
                      offset: const Offset(0, 4),
                      blurRadius: 4,
                    ),
                  ]
                : [],
          ),
          child: child ??
              Center(
                child: AppText(
                  text ?? '',
                  style: AppTypography.font16Montserrat.copyWith(
                    color: isFilled ? Colors.white : Colors.black,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
        ),
      ),
    );
  }
}
