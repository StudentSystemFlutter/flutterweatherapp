import 'package:flutter/material.dart';
import 'package:flutterweatherapp/constants/values/color_constants.dart';
import 'package:flutterweatherapp/constants/values/font_constants.dart';

class AppText extends StatelessWidget {
  final String text;
  final int? maxLines;
  final TextStyle? style;
  final TextAlign? textAlign;
  final TextOverflow? overflow;

  const AppText(
    this.text, {
    Key? key,
    this.maxLines,
    this.style,
    this.textAlign,
    this.overflow,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      maxLines: maxLines,
      textAlign: textAlign,
      overflow: overflow,
      style: style ??
          AppTypography.font14Montserrat.copyWith(
            color: AppColors.black,
          ),
    );
  }
}
