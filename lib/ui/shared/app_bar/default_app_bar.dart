import 'package:flutter/material.dart';
import 'package:flutterweatherapp/constants/values/color_constants.dart';
import 'package:flutterweatherapp/constants/values/font_constants.dart';
import 'package:flutterweatherapp/ui/shared/shared_widgets.dart';

class DefaultAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final Widget? leading;
  final List<Widget>? actions;

  const DefaultAppBar({
    Key? key,
    required this.title,
    this.leading,
    this.actions,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: AppText(
        title,
        style: AppTypography.font16Montserrat.copyWith(
          color: AppColors.white,
          fontWeight: FontWeight.w500,
        ),
      ),
      centerTitle: false,
      backgroundColor: AppColors.accent,
      elevation: 4,
      actions: actions,
    );
  }

  @override
  Size get preferredSize => AppBar().preferredSize;
}
