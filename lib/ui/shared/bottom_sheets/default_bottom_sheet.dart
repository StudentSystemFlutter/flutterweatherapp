import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutterweatherapp/constants/values/color_constants.dart';
import 'package:flutterweatherapp/constants/values/font_constants.dart';
import 'package:flutterweatherapp/ui/shared/borders/border_radius.dart';
import 'package:flutterweatherapp/ui/shared/bottom_sheets/widgets/bottom_sheet_top_icon.dart';
import 'package:flutterweatherapp/ui/shared/shared_widgets.dart';

class DefaultBottomSheet extends StatelessWidget {
  final String title;
  final bool addBottomPadding;
  final List<Widget> children;
  final double heightRatio;

  const DefaultBottomSheet({
    Key? key,
    required this.title,
    required this.children,
    this.addBottomPadding = true,
    this.heightRatio = 0.45,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final double bottom1 = MediaQuery.of(context).padding.bottom;
    final double bottom2 = MediaQuery.of(context).viewInsets.bottom;

    return Container(
      constraints: BoxConstraints(maxHeight: screenHeight * heightRatio),
      padding: EdgeInsets.only(
        top: 8,
        bottom: addBottomPadding ? bottom1 + 16 + bottom2 : 0,
      ),
      decoration: BoxDecoration(
        color: AppColors.accentDark,
        borderRadius: getBorderRadius(
          radius: 24,
          isTopLeftRounded: true,
          isTopRightRounded: true,
        ),
      ),
      child: SingleChildScrollView(
        child: Wrap(
          children: <Widget>[
            const BottomSheetTopIcon(),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 16.w),
              padding: EdgeInsets.symmetric(vertical: 12.h),
              alignment: Alignment.bottomCenter,
              decoration: const BoxDecoration(
                border: Border(
                  bottom: BorderSide(color: AppColors.border, width: .5),
                ),
              ),
              child: AppText(
                title,
                textAlign: TextAlign.center,
                style: AppTypography.font18Montserrat.copyWith(
                  fontWeight: FontWeight.w700,
                  color: AppColors.white,
                ),
              ),
            ),
            const SizedBox(height: 14),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 12.h, horizontal: 16.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: children,
              ),
            ),
            SizedBox(height: bottom1 + 16 + bottom2)
          ],
        ),
      ),
    );
  }
}
