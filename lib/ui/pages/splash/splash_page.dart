import 'package:flutter/material.dart';
import 'package:flutterweatherapp/api/api_provider.dart';
import 'package:flutterweatherapp/constants/assets/image_constants.dart';
import 'package:flutterweatherapp/constants/values/color_constants.dart';
import 'package:flutterweatherapp/constants/values/route_constants.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutterweatherapp/utils/shared_preference/shared_pref_helper.dart';
import 'package:easy_localization/easy_localization.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    ApiProvider.create();

    Future.delayed(
      const Duration(milliseconds: 1500),
      () async {
        Navigator.of(context).pushNamedAndRemoveUntil(
          AppRoutes.homePage,
          (route) => false,
        );
      },
    );

    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    final locale = WidgetsBinding.instance?.window.locale;

    if (locale != null) {
      final lang = locale.languageCode;

      if (lang == 'ru') {
        SharedPrefHelper.language = lang;
        context.setLocale(const Locale('ru', 'RU'));
      } else {
        SharedPrefHelper.language = 'en';
        context.setLocale(const Locale('en', 'US'));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.accent,
      body: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 12.w),
          child: Image.asset(AppImages.logo, width: 275.w),
        ),
      ),
    );
  }
}
