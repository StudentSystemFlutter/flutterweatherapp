import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterweatherapp/bloc/auth/auth_bloc.dart';
import 'package:flutterweatherapp/bloc/home/home_bloc.dart';
import 'package:flutterweatherapp/constants/values/color_constants.dart';
import 'package:flutterweatherapp/constants/values/font_constants.dart';
import 'package:flutterweatherapp/ui/pages/home/widgets/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutterweatherapp/ui/shared/loaders/default_loader.dart';
import 'package:flutterweatherapp/ui/shared/shared_widgets.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();

    context.read<AuthBloc>().checkAuth();
    context.read<HomeBloc>().loadWeatherDataForCurrentLocation();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.accent,
      body: BlocBuilder<HomeBloc, HomeState>(
        builder: (context, state) {
          if (state.isLoading) {
            return const DefaultLoader();
          }

          return SafeArea(
            bottom: false,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const AuthPanel(),
                  SizedBox(height: 14.h),
                  AppText(
                    tr('today'),
                    style: AppTypography.font17Montserrat.copyWith(
                      color: AppColors.white,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  SizedBox(height: 12.h),
                  const CurrentWeatherSection(),
                  SizedBox(height: 22.h),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: AppText(
                          state.locationText,
                          maxLines: 3,
                          style: AppTypography.font15Montserrat.copyWith(
                            color: AppColors.white,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      SizedBox(width: 10.w),
                      const Align(
                        alignment: Alignment.topRight,
                        child: WeatherTypeDropdown(),
                      ),
                    ],
                  ),
                  SizedBox(height: 15.h),
                  const Expanded(child: WeatherList()),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
