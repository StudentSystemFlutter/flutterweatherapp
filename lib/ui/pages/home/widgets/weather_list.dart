import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterweatherapp/bloc/home/home_bloc.dart';
import 'package:flutterweatherapp/ui/pages/home/widgets/widgets.dart';
import 'package:flutterweatherapp/ui/shared/shared_widgets.dart';
import 'package:flutterweatherapp/utils/icon_selector/icon_selector.dart';

class WeatherList extends StatelessWidget {
  const WeatherList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (context, state) {
        if (state.isLoading) {
          return const DefaultLoader();
        }

        if (state.selectedWeatherType.type == 0) {
          if (state.hourlyDataList.isEmpty) {
            return const SizedBox.shrink();
          }

          return ListView.builder(
            itemCount: state.hourlyDataList.length,
            physics: const BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              final model = state.hourlyDataList[index];
              final iconUrl = getWeatherAppIcon(iconName: model.iconName);

              return WeatherListHourlyItem(iconUrl: iconUrl, model: model);
            },
          );
        } else {
          if (state.dailyDataList.isEmpty) {
            return const SizedBox.shrink();
          }

          return ListView.builder(
            itemCount: state.dailyDataList.length,
            physics: const BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              final model = state.dailyDataList[index];
              final iconUrl = getWeatherAppIcon(iconName: model.iconName);

              return WeatherListDailyItem(iconUrl: iconUrl, model: model);
            },
          );
        }
      },
    );
  }
}
