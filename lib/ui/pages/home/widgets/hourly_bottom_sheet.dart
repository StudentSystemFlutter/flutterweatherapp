import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutterweatherapp/constants/values/color_constants.dart';
import 'package:flutterweatherapp/constants/values/font_constants.dart';
import 'package:flutterweatherapp/models/view/hourly_data_view_model.dart';
import 'package:flutterweatherapp/ui/shared/bottom_sheets/default_bottom_sheet.dart';
import 'package:flutterweatherapp/ui/shared/shared_widgets.dart';
import 'package:flutterweatherapp/utils/text/string_extension.dart';

class HourlyBottomSheet extends StatelessWidget {
  final HourlyDataViewModel model;

  const HourlyBottomSheet({
    Key? key,
    required this.model,
  }) : super(key: key);

  static Future<void> show(
    BuildContext context, {
    required HourlyDataViewModel model,
  }) async {
    return showModalBottomSheet(
      context: context,
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: getBorderRadius(
          radius: 24,
          isTopLeftRounded: true,
          isTopRightRounded: true,
        ),
      ),
      isDismissible: true,
      isScrollControlled: true,
      builder: (context) {
        return HourlyBottomSheet(model: model);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultBottomSheet(
      title: model.day + ' ' + model.time,
      heightRatio: 0.6,
      children: [
        AppText(
          '${model.temperature}°C',
          textAlign: TextAlign.center,
          style: AppTypography.font40Montserrat.copyWith(
            height: 1.2,
            color: AppColors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
        SizedBox(height: 10.h),
        AppText(
          model.weatherStatus.capitalize(),
          style: AppTypography.font15Montserrat.copyWith(
            color: AppColors.white,
          ),
        ),
        SizedBox(height: 5.h),
        AppText(
          tr('humidity') + ': ${model.humidity}%',
          style: AppTypography.font15Montserrat.copyWith(
            color: AppColors.white,
          ),
        ),
        SizedBox(height: 5.h),
        AppText(
          tr('pressure') + ': ${model.pressure}',
          style: AppTypography.font15Montserrat.copyWith(
            color: AppColors.white,
          ),
        ),
        SizedBox(height: 5.h),
        AppText(
          tr('clouds') + ': ${model.clouds}',
          style: AppTypography.font15Montserrat.copyWith(
            color: AppColors.white,
          ),
        ),
        SizedBox(height: 5.h),
        AppText(
          tr('visibility') + ': ${model.visibility}',
          style: AppTypography.font15Montserrat.copyWith(
            color: AppColors.white,
          ),
        ),
        SizedBox(height: 5.h),
        AppText(
          tr('windSpeed') + ': ${model.windSpeed}',
          style: AppTypography.font15Montserrat.copyWith(
            color: AppColors.white,
          ),
        ),
      ],
    );
  }
}
