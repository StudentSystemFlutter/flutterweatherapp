import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutterweatherapp/constants/assets/icon_constants.dart';
import 'package:flutterweatherapp/constants/values/color_constants.dart';
import 'package:flutterweatherapp/constants/values/font_constants.dart';
import 'package:flutterweatherapp/ui/shared/shared_widgets.dart';

class GoogleSignInButton extends StatelessWidget {
  final VoidCallback onTap;

  const GoogleSignInButton({Key? key, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 42.h,
        decoration: BoxDecoration(
          color: AppColors.blueColor,
          borderRadius: getBorderRadius(),
        ),
        child: Row(
          children: [
            SizedBox(width: 18.w),
            SvgPicture.asset(
              AppIcons.google,
              width: 26.w,
              color: AppColors.white,
            ),
            SizedBox(width: 24.w),
            AppText(
              'Sign in with Google',
              style: AppTypography.font18Montserrat.copyWith(
                color: AppColors.white,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AppleSignInButton extends StatelessWidget {
  final VoidCallback onTap;

  const AppleSignInButton({Key? key, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 42.h,
        decoration: BoxDecoration(
          color: AppColors.black,
          borderRadius: getBorderRadius(),
        ),
        child: Row(
          children: [
            SizedBox(width: 18.w),
            SvgPicture.asset(
              AppIcons.apple,
              width: 26.w,
            ),
            SizedBox(width: 24.w),
            AppText(
              'Sign in with Apple',
              style: AppTypography.font18Montserrat.copyWith(
                color: AppColors.white,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
