import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterweatherapp/bloc/home/home_bloc.dart';
import 'package:flutterweatherapp/constants/values/color_constants.dart';
import 'package:flutterweatherapp/constants/values/font_constants.dart';
import 'package:flutterweatherapp/models/view/weather_type.dart';
import 'package:flutterweatherapp/ui/shared/shared_widgets.dart';
import 'package:flutterweatherapp/utils/validator/validators.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WeatherTypeDropdown extends StatefulWidget {
  const WeatherTypeDropdown({Key? key}) : super(key: key);

  @override
  _WeatherTypeDropdownState createState() => _WeatherTypeDropdownState();
}

class _WeatherTypeDropdownState extends State<WeatherTypeDropdown> {
  final weatherTypesList = <WeatherType>[
    WeatherType(type: 0, text: tr('hourly')),
    WeatherType(type: 1, text: tr('daily'))
  ];

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (context, state) {
        return Container(
          padding: EdgeInsets.only(left: 10.w, right: 5.w),
          decoration: BoxDecoration(
            color: AppColors.white,
            borderRadius: getBorderRadius(),
          ),
          child: DropdownButton<String>(
            value: state.selectedWeatherType.text,
            icon: const Icon(
              Icons.arrow_drop_down,
              color: AppColors.accentDark,
            ),
            iconSize: 24,
            elevation: 16,
            style: const TextStyle(color: AppColors.accentDark),
            underline: Container(
              height: 2,
              color: AppColors.white,
            ),
            dropdownColor: AppColors.white,
            onChanged: (String? newValue) {
              if (Validators.isStringNotEmpty(newValue)) {
                if (newValue == weatherTypesList[0].text) {
                  context
                      .read<HomeBloc>()
                      .changeWeatherType(weatherTypesList[0]);
                } else {
                  context
                      .read<HomeBloc>()
                      .changeWeatherType(weatherTypesList[1]);
                }
              }
            },
            items: weatherTypesList.map<DropdownMenuItem<String>>(
              (weatherType) {
                return DropdownMenuItem<String>(
                  value: weatherType.text,
                  child: AppText(
                    weatherType.text,
                    style: AppTypography.font14Montserrat.copyWith(
                      fontWeight: FontWeight.w600,
                      color: weatherType.type == state.selectedWeatherType.type
                          ? AppColors.accentDark
                          : AppColors.greyColor,
                    ),
                  ),
                );
              },
            ).toList(),
          ),
        );
      },
    );
  }
}
