import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutterweatherapp/constants/values/color_constants.dart';

class WeatherIcon extends StatelessWidget {
  final String iconUrl;
  final double? size;
  final Color? backgroundColor;

  const WeatherIcon({
    Key? key,
    required this.iconUrl,
    this.size = 80,
    this.backgroundColor = AppColors.accentLight,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset(iconUrl, width: size),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: backgroundColor,
      ),
      padding: EdgeInsets.all(10.w),
    );
  }
}
