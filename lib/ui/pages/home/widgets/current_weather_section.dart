import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterweatherapp/bloc/home/home_bloc.dart';
import 'package:flutterweatherapp/constants/values/color_constants.dart';
import 'package:flutterweatherapp/constants/values/font_constants.dart';
import 'package:flutterweatherapp/ui/pages/home/widgets/widgets.dart';
import 'package:flutterweatherapp/ui/shared/shared_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutterweatherapp/utils/icon_selector/icon_selector.dart';

class CurrentWeatherSection extends StatelessWidget {
  const CurrentWeatherSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (context, state) {
        return Container(
          height: 90.h,
          padding: EdgeInsets.all(10.w),
          decoration: BoxDecoration(
            color: AppColors.white,
            borderRadius: getBorderRadius(),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              WeatherIcon(
                size: 64.w,
                backgroundColor: AppColors.accent,
                iconUrl: getWeatherAppIcon(
                  iconName: state.currentWeatherViewModel.iconName,
                ),
              ),
              SizedBox(width: 15.w),
              Expanded(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    AppText(
                      state.currentWeatherViewModel.temperature,
                      style: AppTypography.font40Montserrat.copyWith(
                        fontWeight: FontWeight.w700,
                        color: AppColors.accent,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 14.h),
                      child: AppText(
                        '°C',
                        style: AppTypography.font14Montserrat.copyWith(
                          fontWeight: FontWeight.w700,
                          color: AppColors.accent,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  AppText(
                    tr('humidity') +
                        ': ${state.currentWeatherViewModel.humidity}%',
                    style: AppTypography.font13Montserrat.copyWith(
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(height: 5.h),
                  AppText(
                    tr('pressure') +
                        ': ${state.currentWeatherViewModel.pressure}',
                    style: AppTypography.font13Montserrat.copyWith(
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(height: 5.h),
                  AppText(
                    tr('windSpeed') +
                        ': ${state.currentWeatherViewModel.windSpeed}',
                    style: AppTypography.font13Montserrat.copyWith(
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(height: 5.h),
                  AppText(
                    tr('clouds') + ': ${state.currentWeatherViewModel.clouds}',
                    style: AppTypography.font13Montserrat.copyWith(
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
