import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutterweatherapp/constants/values/color_constants.dart';
import 'package:flutterweatherapp/constants/values/font_constants.dart';
import 'package:flutterweatherapp/models/view/hourly_data_view_model.dart';
import 'package:flutterweatherapp/ui/pages/home/widgets/hourly_bottom_sheet.dart';
import 'package:flutterweatherapp/ui/pages/home/widgets/widgets.dart';
import 'package:flutterweatherapp/ui/shared/shared_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutterweatherapp/utils/text/string_extension.dart';

class WeatherListHourlyItem extends StatelessWidget {
  final String iconUrl;
  final HourlyDataViewModel model;

  const WeatherListHourlyItem({
    Key? key,
    required this.iconUrl,
    required this.model,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        HourlyBottomSheet.show(context, model: model);
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 6.w),
        padding: EdgeInsets.all(10.w),
        height: 93.h,
        decoration: BoxDecoration(
          color: AppColors.accentDark,
          borderRadius: getBorderRadius(),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                AppText(
                  '${model.day} ${model.time}',
                  style: AppTypography.font16Montserrat.copyWith(
                    color: AppColors.white,
                  ),
                ),
                const Spacer(),
                Icon(Icons.arrow_right_alt, size: 22.w),
              ],
            ),
            SizedBox(height: 7.h),
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  WeatherIcon(iconUrl: iconUrl, size: 24),
                  SizedBox(width: 10.w),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AppText(
                        model.weatherStatus.capitalize(),
                        style: AppTypography.font14Montserrat.copyWith(
                          color: AppColors.white,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(height: 5.h),
                      AppText(
                        tr('temperature') + ': ${model.temperature}°C',
                        style: AppTypography.font12Montserrat.copyWith(
                          color: AppColors.white,
                        ),
                      ),
                      SizedBox(height: 2.h),
                      AppText(
                        tr('humidity') + ': ${model.humidity}%',
                        style: AppTypography.font12Montserrat.copyWith(
                          color: AppColors.white,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
