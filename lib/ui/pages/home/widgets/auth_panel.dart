import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterweatherapp/bloc/auth/auth_bloc.dart';
import 'package:flutterweatherapp/constants/values/color_constants.dart';
import 'package:flutterweatherapp/constants/values/font_constants.dart';
import 'package:flutterweatherapp/ui/shared/shared_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AuthPanel extends StatelessWidget {
  const AuthPanel({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45.h,
      alignment: Alignment.topLeft,
      padding: EdgeInsets.only(top: 10.h),
      child: BlocBuilder<AuthBloc, AuthState>(
        builder: (context, state) {
          if (state.isLoading) {
            return const DefaultLoader();
          }

          if (!state.isAuthenticated) {
            return IconButton(
              onPressed: () {
                context.read<AuthBloc>().signInWithGoogle();
              },
              icon: const Icon(
                Icons.person,
                color: AppColors.white,
              ),
            );
          }

          return AppText(
            tr('hello') + ',\n' + state.displayName,
            maxLines: 2,
            style: AppTypography.font16Montserrat.copyWith(
              color: AppColors.white,
            ),
          );
        },
      ),
    );
  }
}
