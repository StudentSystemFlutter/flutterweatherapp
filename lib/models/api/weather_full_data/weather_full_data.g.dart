// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_full_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WeatherFullDataResponse _$WeatherFullDataResponseFromJson(
        Map<String, dynamic> json) =>
    WeatherFullDataResponse(
      lat: (json['lat'] as num?)?.toDouble(),
      lon: (json['lon'] as num?)?.toDouble(),
      timezone: json['timezone'] as String?,
      timezoneOffset: json['timezone_offset'] as int?,
      current: json['current'] == null
          ? null
          : CurrentWeatherResponse.fromJson(
              json['current'] as Map<String, dynamic>),
      hourly: (json['hourly'] as List<dynamic>?)
          ?.map(
              (e) => HourlyWeatherResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      daily: (json['daily'] as List<dynamic>?)
          ?.map((e) => DailyWeatherResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$WeatherFullDataResponseToJson(
        WeatherFullDataResponse instance) =>
    <String, dynamic>{
      'lat': instance.lat,
      'lon': instance.lon,
      'timezone': instance.timezone,
      'timezone_offset': instance.timezoneOffset,
      'current': instance.current?.toJson(),
      'hourly': instance.hourly?.map((e) => e.toJson()).toList(),
      'daily': instance.daily?.map((e) => e.toJson()).toList(),
    };
