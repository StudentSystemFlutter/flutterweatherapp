class CurrentWeatherViewModel {
  final String iconName;
  final String weatherStatus;
  final String temperature;
  final String humidity;
  final String pressure;
  final String clouds;
  final String windSpeed;

  CurrentWeatherViewModel({
    required this.iconName,
    required this.weatherStatus,
    required this.temperature,
    required this.humidity,
    required this.pressure,
    required this.clouds,
    required this.windSpeed,
  });
}
