class WeatherType {
  final int type; // 0 for Hourly, 1 for daily
  final String text;

  WeatherType({
    required this.type,
    required this.text,
  });
}
