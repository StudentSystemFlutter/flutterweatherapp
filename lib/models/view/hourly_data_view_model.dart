class HourlyDataViewModel {
  final String iconName;
  final String day;
  final String time;
  final String weatherStatus;
  final String temperature;
  final String humidity;
  final String pressure;
  final String clouds;
  final String visibility;
  final String windSpeed;

  HourlyDataViewModel({
    required this.iconName,
    required this.day,
    required this.time,
    required this.weatherStatus,
    required this.temperature,
    required this.humidity,
    required this.pressure,
    required this.clouds,
    required this.visibility,
    required this.windSpeed,
  });
}
